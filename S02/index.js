//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//Spaghetti Code - When code is not organized enough that it becomes hard to work it.
//Encapsulation - Organizing of related information(properties) and behaviour(methods) to belong to a single entity.

//Encapsulate the following information into 4 student objects using object literals:

/*
    Mini-Activity
    Update the logout and listGrades methods of studentOne to be able to show studentOne's email with our message and the grades of studentOne.
*/


/*//create student one
    let studentOneName = 'John';
    let studentOneEmail = 'john@mail.com';
    let studentOneGrades = [89, 84, 78, 88];

//create student two
    let studentTwoName = 'Joe';
    let studentTwoEmail = 'joe@mail.com';
    let studentTwoGrades = [78, 82, 79, 85];

//create student three
    let studentThreeName = 'Jane';
    let studentThreeEmail = 'jane@mail.com';
    let studentThreeGrades = [87, 89, 91, 93];

//create student four
    let studentFourName = 'Jessie';
    let studentFourEmail = 'jessie@mail.com';
    let studentFourGrades = [91, 89, 92, 93];*/

//actions that students may perform will be lumped together
    function login(email){
        console.log(`${email} has logged in`);
    }

    function logout(email){
        console.log(`${email} has logged out`);
    }

    function listGrades(grades){
        grades.forEach(grade => {
            console.log(grade);
        })
    }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

//QUIZ
/*
    1. What is the term given to unorganized code that's very hard to work with? 
        - Spaghetti code
    2. How are object literals written in JS?
        - let objectName = {
            property: value,
            property2: value2
        }
    3. What do you call the concept of organizing information and functionality to belong to an object?
        - Encapsulation
    4. If student1 has a method named enroll(), how would you invoke it?
        -student1.enroll()
    5. True or False: Objects can have objects as properties.
        -True
    6. What does this keyword refer to if used in an arrow function method?
        -global property
    7. True or False: A method can have no parameters and still work.
        -True
    8. True or False: Arrays can have objects as elements.
        -False
    9. True or False: Arrays are objects
        -True
    10. True or False: Objects can havee arrays as properties.
        -True
*/
//Function Coding

    let studentOne = {
        name: 'John',
        email: 'john@mail.com',
        grades: [89, 84, 78, 88],
        average: 0,
        login: function(){
            //login should allow our student object to display its own email with our message.
            //What is this in the context of an object method?
            //this, when inside a method refers to the object where it is in.
            console.log(`${this.email} has logged in`);
        },
        logout: function(){
             console.log(`${this.email} has logged out`);
        },
        listGrades: function(){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
        computeAve: function(){
            this.average = this.grades.reduce((a,b) => a+b, 0) / this.grades.length;
            console.log(this.average);
        },
        willPass: function(){
            if(Math.round(this.average) >= 85) {
                console.log(true)
            } else {
                console.log(false)
            }
        },
        willPassWithHonors: function(){
            if(Math.round(this.average) >= 90){
                console.log(true)
            } else if(Math.round(this.average) >= 85 && Math.round(this.average) < 90) {
                console.log(false)
            } else {
                console.log(undefined)
            };
        }
    };

    let studentTwo = {
        name: 'Joe',
        email: 'joe@mail.com',
        grades: [78, 82, 79, 85],
        average: 0,
        login: function(){
            console.log(`${this.email} has logged in`);
        },
        logout: function(){
             console.log(`${this.email} has logged out`);
        },
        listGrades: function(){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
        computeAve: function(){
            this.average = this.grades.reduce((a,b) => a+b, 0) / this.grades.length;
            console.log(this.average);
        },
        willPass: function(){
            if(Math.round(this.average) >= 85) {
                console.log(true)
            } else {
                console.log(false)
            }
        },
        willPassWithHonors: function(){
            if(Math.round(this.average) >= 90){
                console.log(true)
            } else if(Math.round(this.average) >= 85 && Math.round(this.average) < 90) {
                console.log(false)
            } else {
                console.log(undefined)
            };
        }
    };

    let studentThree = {
        name: 'Jane',
        email: 'jane@mail.com',
        grades: [87, 89, 91, 93],
        average: 0,
        login: function(){
            console.log(`${this.email} has logged in`);
        },
        logout: function(){
             console.log(`${this.email} has logged out`);
        },
        listGrades: function(){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
        computeAve: function(){
            this.average = this.grades.reduce((a,b) => a+b, 0) / this.grades.length;
            console.log(this.average);
        },
        willPass: function(){
            if(Math.round(this.average) >= 85) {
                console.log(true)
            } else {
                console.log(false)
            }
        },
        willPassWithHonors: function(){
            if(Math.round(this.average) >= 90){
                console.log(true)
            } else if(Math.round(this.average) >= 85 && Math.round(this.average) < 90) {
                console.log(false)
            } else {
                console.log(undefined)
            };
        }
    };

    let studentFour = {
        name: 'Jessie',
        email: 'jessie@mail.com',
        grades: [91, 89, 92, 93],
        average: 0,
        login: function(){
            console.log(`${this.email} has logged in`);
        },
        logout: function(){
             console.log(`${this.email} has logged out`);
        },
        listGrades: function(){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
        computeAve: function(){
            this.average = this.grades.reduce((a,b) => a+b, 0) / this.grades.length;
            console.log(this.average);
        },
        willPass: function(){
            if(Math.round(this.average) >= 85) {
                console.log(true)
            } else {
                console.log(false)
            }
        },
        willPassWithHonors: function(){
            if(Math.round(this.average) >= 90){
                console.log(true)
            } else if(Math.round(this.average) >= 85 && Math.round(this.average) < 90) {
                console.log(false)
            } else {
                console.log(undefined)
            };
        }
    };